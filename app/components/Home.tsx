import React, { useState } from 'react';
import styled from 'styled-components';
import { AutoComplete } from 'antd';

export default function Home() {
  const mockVal = (str: string, repeat: number = 1) => {
    return {
      value: str.repeat(repeat)
    };
  };
  const [value, setValue] = useState('');
  const [options, setOptions] = useState<{ value: string }[]>([]);
  const onSearch = (searchText: string) => {
    setOptions(
      !searchText
        ? []
        : [mockVal(searchText), mockVal(searchText, 2), mockVal(searchText, 3)]
    );
  };
  const onSelect = (data: string) => {
    console.log('onSelect', data);
  };
  const onChange = (data: string) => {
    setValue(data);
  };
  return (
    <Wrapper>
      {/* <WrapSearch>
      </WrapSearch> */}
      <AutoComplete
        // dropdownStyle={{
        //   backgroundColor: '#d7dfe3',
        //   borderBottomLeftRadius: 30,
        //   borderBottomRightRadius: 30,
        //   color: 'black',
        //   paddingLeft: 20
        // }}
     
        options={options}
        style={{
          width: '100%',
          height: 50,
          borderRadius: 30,
          backgroundColor: 'green',
          justifyContent: 'center',
          alignItems: 'center'
        }}
        
        onSelect={onSelect}
        onSearch={onSearch}
      />
    </Wrapper>
  );
}
const Wrapper = styled.div`
  background: linear-gradient(45deg, #2979ff, #1051bd);
  align-items: center;
  justify-content: center;
  padding: 20px;
  width: 100%
`;
const WrapSearch = styled.div`
  background: white;
  width: 70%;
  border-radius: 30px;
  height: 50px;
`;
const Text = styled.text`
  color: red;
`;
